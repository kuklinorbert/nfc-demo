import 'package:flutter/material.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:nfc_manager/platform_tags.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'NFC Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
    String handle = "";
    String data = "";
    bool isAvailable = false;
    bool isEnabled = true;


    void startSession() async{
      isAvailable = await NfcManager.instance.isAvailable();
      if(isAvailable){
        setState(() {
          isEnabled = false;
        });
        NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
          setState(() {
            handle = tag.handle;
            data = tag.data.toString();
         });
        } );
      }else{
        ScaffoldMessenger.of(context).showSnackBar(new SnackBar(content: Text("NFC not available")));
      }
      }

      void stopSession(){
        setState(() {
          isEnabled = true;
        });
        NfcManager.instance.stopSession();
      }
 
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center( 
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(onPressed: isEnabled ? startSession : null, child: Text("Start searching"),),
            ElevatedButton(onPressed: isEnabled ? null : stopSession, child:  Text("Stop searching")),
            Text("Handle: " + handle),
            Text("Data: " + data),
          ],
        ),
      ),
      
    );
  }
}
